
## Install dependencies

```
npm install
npm run bootstrap
```

## Run locally
```
npm start
```

## Deploy

Prerequisites:
- Set AWS credentials

```

npm run deploy
```