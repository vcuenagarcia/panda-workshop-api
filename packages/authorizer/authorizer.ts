import { APIGatewayTokenAuthorizerEvent, APIGatewayAuthorizerEvent, APIGatewayAuthorizerHandler, APIGatewayAuthorizerResult } from 'aws-lambda'
import jwt from 'jsonwebtoken';
import jwksRsa from 'jwks-rsa';

const jwksClient = jwksRsa({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 10,
    jwksUri: 'https://cimpress.auth0.com/.well-known/jwks.json'
});

const generatePolicy = (principalId: string, resource: string) => {
    return {
        principalId,
        policyDocument: {
            Version: '2012-10-17',
            Statement: [{
                Action: 'execute-api:Invoke',
                Effect: 'Allow',
                Resource: resource
            }],
        }
    };
}

export const handler: APIGatewayAuthorizerHandler = async (event: APIGatewayAuthorizerEvent): Promise<APIGatewayAuthorizerResult> => {

    try {
        const token = (event as APIGatewayTokenAuthorizerEvent).authorizationToken?.replace('Bearer ', '')
        const decodedToken = jwt.decode(token, { complete: true });

        if (!decodedToken || !decodedToken.payload?.sub) {
            throw new Error('Unauthorized');
        }

        const kid = decodedToken.header?.kid;
        const key = await jwksClient.getSigningKey(kid);
        const signingKey = key.getPublicKey();

        jwt.verify(token, signingKey, {
            issuer: 'https://cimpress.auth0.com/',
            audience: 'https://api.cimpress.io/'
        });
        const policy = generatePolicy(decodedToken.payload.sub, event.methodArn);

        return policy;
    } catch (error) {
        console.error(error);
        throw new Error('Unauthorized');
    }
}