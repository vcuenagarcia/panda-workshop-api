import { Request, Response, NextFunction } from 'express';
import jwt from 'jsonwebtoken';

export interface UserData {
    accessToken: string;
    principalId: string;
    account: string;
    email: string;
}

export interface ResponseWithUserData extends Response {
    locals: {
        userAuth: UserData;
    }
}

export const appendUserDataToLocals = (req: Request, res: Response, next: NextFunction) => {

    try {
        const authorizationHeader = req.headers.authorization;
        const decodedToken = authorizationHeader
            ? jwt.decode(authorizationHeader.replace('Bearer ', '')) as Record<string, string>
            : {};

        res.locals.userAuth = {
            accessToken: authorizationHeader || 'n/a',
            principalId: decodedToken.sub || 'n/a',
            account: decodedToken['https://claims.cimpress.io/account'] || 'n/a',
            email: decodedToken['https://claims.cimpress.io/email'] || 'n/a'
        };

        return next();
    } catch (error) {
        error.status = 403;
        console.error(error);
        return next(error);
    }
}
