import { Request } from "express"
import { ResponseWithUserData } from "../auth";

export const getBooks = async (req: Request, res: ResponseWithUserData): Promise<ResponseWithUserData | void> => {
    console.log(res.locals.userAuth);
    return res
        .status(200)
        .send(`These are your books, ${res.locals.userAuth.email}`);
}