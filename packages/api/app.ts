import express from 'express';
import { appendUserDataToLocals } from './auth';
import { getBooks } from './controllers/BooksController';

const app = express();

const router = express.Router();

// Routes
router.get('/v0/books', getBooks);

app.use(appendUserDataToLocals);
app.use('/', router);

export default app;