// eslint-disable-next-line @typescript-eslint/no-var-requires
const path = require('path');

module.exports = {
    target: 'node',
    mode: 'production',
    optimization: {
        minimize: true,
        sideEffects: false,
    },
    context: path.resolve(__dirname),
    entry: './index.js',
    externals: ['aws-sdk', 'aws-lambda'], // provided by the AWS Lambda runtime, no need to bundle
    output: {
        libraryTarget: 'commonjs',
        filename: 'index.js',
        path: path.resolve(__dirname, 'build'),
    },
    module: {
        rules: [
            {
                test: /\.ya?ml$/,
                type: 'asset/resource',
                generator: {
                    filename: '[path][name][ext]'
                  }
            }
        ]
    }
};