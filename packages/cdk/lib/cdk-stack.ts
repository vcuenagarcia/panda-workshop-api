import * as cdk from '@aws-cdk/core';
import { IdentitySource, LambdaRestApi, RequestAuthorizer, TokenAuthorizer } from '@aws-cdk/aws-apigateway';
import { Effect, PolicyStatement, Role, ServicePrincipal } from '@aws-cdk/aws-iam';
import { Function, Runtime, Code } from '@aws-cdk/aws-lambda';
import { Tags } from '@aws-cdk/core';

export class CdkStack extends cdk.Stack {
  constructor(scope: cdk.Construct) {
    super(scope);

    // IAM Role
    const roleId = 'LambdaAPIDemo';
    const role = new Role(this, roleId, {
      assumedBy: new ServicePrincipal('lambda.amazonaws.com'),
      roleName: roleId
    });

    role.addToPolicy(new PolicyStatement({
      effect: Effect.ALLOW,
      actions: ['logs:CreateLogGroup', 'logs:CreateLogStream', 'logs:PutLogEvents'],
      resources: ['*']
    }));


    // Authorizer
    const authorizerFn = new Function(this, 'demoAuthorizerFunction', {
      functionName: 'demoAuthorizerFunction',
      runtime: Runtime.NODEJS_14_X,
      handler: 'authorizer.handler',
      code: Code.fromAsset('packages/authorizer'),
      role: role,
    });

    const authorizer = new TokenAuthorizer(this, 'demoAuthorizer', {
      handler: authorizerFn,
      authorizerName: 'demoAuthorizer'
    });

    // API Gateway
    
    const apiLambda = new Function(this, 'DemoLambdaRestAPI', {
      functionName: 'DemoLambdaRestAPI',
      code: Code.fromAsset('packages/api/build'),
      runtime: Runtime.NODEJS_14_X,
      handler: 'index.handler',
      role: role
    });

    const api = new LambdaRestApi(this, 'DemoRestAPI', {
      restApiName: 'DemoRestAPI',
      handler: apiLambda,
      proxy: true,
      defaultMethodOptions: {
        authorizer
      }
    });

    authorizer._attachToApi(api);

    // TAGS
    const tags = {
      Service: "Demo API",
      Organization: "Cimpress",
      Domain: "Data",
      Squad: "Panda"
    }

    Object.entries(tags).forEach(
      ([key, value]) => Tags.of(this).add(key, value)
    );
  }
}
