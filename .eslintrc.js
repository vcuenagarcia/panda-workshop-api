module.exports = {
  parser: '@typescript-eslint/parser',
  parserOptions: {
    ecmaVersion: 2019,
    sourceType: 'module'
  },
  plugins: [
    'import',
    '@typescript-eslint/eslint-plugin'
  ],
  env: {
    es6: true,
    node: true
  },
  extends: [
    'eslint:recommended',
    'plugin:import/typescript',
    'plugin:@typescript-eslint/recommended'
  ],
  settings: {},
  rules: {
    "no-useless-escape": 0,
    "no-underscore-dangle": 0,
    "no-console": "error"
      }
};
