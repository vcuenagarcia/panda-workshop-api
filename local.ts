import app from './packages/api/app';

const port = 3000;
app.listen(port);

// eslint-disable-next-line no-console
console.log(`listening on http://localhost:${port}`);
